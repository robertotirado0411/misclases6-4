/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author Edgar Guerrero   
 */
public class Gasolina {
    
private int tipo;
private float precio;
private String marca ;
private int id;

    public Gasolina() {
        this.tipo=0;
        this.precio=0.0f;
        this.id=0;
        this.marca="";
    }


    public Gasolina(int tipo, float precio, int id, String marca) {
        this.tipo = tipo;
        this.precio = precio;
        this.id = id;
        this.marca= marca;
    }
    
    public Gasolina(Gasolina gasolina){
        this.tipo = gasolina.tipo;
        this.precio = gasolina.precio;
        this.id = gasolina.id;
        this.marca= gasolina.marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
            
    
    
    public String MostrarInformacion(){
        String informacion="";
        informacion="id: "+ this.id + " tipo: "+this.tipo + " marca: " +this.marca + " precio : " + this.precio;
        return informacion;
        
        
    }
    
}
